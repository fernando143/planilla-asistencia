# planilla-asistencia

> A Vue.js project

## planillaAsistencia.vue
``` bash
<template>     
    <table class="reportePlanillaAsis">
        <thead class="reportePlanillaAsis__head">
            <tr class="reportePlanillaAsis__tr">
                <td class="reportePlanillaAsis__td reportePlanillaAsis__td--head" v-for="(item, index) in header" :key="index+'a'" v-html="item"></td>
            </tr>
        </thead>
        <tbody>
            <tr class="reportePlanillaAsis__tr" v-for="(alumno, index) in body" :key="index+'b'">
                <td class="reportePlanillaAsis__td" v-for="(item, index) in alumno" :key="index+'c'">{{item}}</td>
            </tr>
        </tbody>
    </table>    
</template>

<script>
export default {    
    props: ['year', 'alumnos', 'month', 'filler'],
    data() {
        return {      
            nameDays: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],            
            nameFirstDay: '',
            daysInMonth: null,
            header: null,            
            body: [],    
            
        }
    },
    mounted() {            
        this.firstDayMonth(this.year, this.month, 1)
        this.daysMonth(this.year, this.month+1, 0)    
        this.constructHeader()
        this.getAlumnos()     
    },
    methods: {            
        firstDayMonth(year, month, day) {        
            let dt = new Date(year, month, day);
            this.nameFirstDay = this.nameDays[dt.getUTCDay()]        
        },
        daysMonth(year, month, day) {
            let dt = new Date(year, month, day)
            let days = dt.getDate()
            this.daysInMonth = days
            
        },    
        constructHeader() {
            let days = [],        
            posFirstDay = null
            //poblar array days con semanas
            for(let i=0; i<5; i++){
                this.nameDays.forEach(e => {                    
                    days.push(e)
                })
            }                    
            //detectar el primer dia del mes
            posFirstDay = days.indexOf(`${this.nameFirstDay}`)
            //eliminar los dias sobrantes anteriores al primer dia del mes
            days.splice(0, posFirstDay)
            //eliminar los dias sobrantes despues del ultimo dia del mes
            let daysDelete = days.length - this.daysInMonth
            if(daysDelete > 0) days.splice(-daysDelete)
            
            //añadir fecha a cada día
            days.forEach((e, i) =>{
                days[i] = `${e}<br> ${i+1}`
            })
            //añadir NOMBRES al principio y ASISTENCIA, FALTAS al final
            days.unshift('NOMBRES')
            days.push('ASIS', 'INASIST')
            this.header = days         
                        
        },
        getAlumnos() {                
            let getAlumnos = this.alumnos,        
            alumnos = []
            
            getAlumnos.forEach(e => {            
                let nameCompleto = [`${e.apellidoAlumno} ${e.nameAlumno}`]
                let asistencias = []           
                for(let i = 0; i < e.registroAsistencia.length; i++){              
                    asistencias.push(e.registroAsistencia[i].value)
                }                       
                asistencias.push(e.asistencias, e.inasistencias)
                alumnos.push(nameCompleto.concat(asistencias))
            })          
            this.constructBody(alumnos)
        },
        constructBody(alumnos) {
            //1 = NOMBRE + diasDelMes
            let lengthRow = 1 + this.daysInMonth,
            lengthAlumno = null,
            result = null,
            //numberAsis para guardar temporalmente numeros de asistencias y luego insertarlas al final
            numberAsis = [],
            readyAlumnos = [] //listos para mandar al model

            alumnos.forEach(alumno => {

                //guardar los ultimos 2 elem de cada alumno
                numberAsis = alumno.splice(-2, 2)            
                lengthAlumno = alumno.length            

                //itera por cada item dentro de un alumno y si es sab o dom agrega un ''            
                for(let a = 0; a < lengthAlumno; a++){              
                    let dia = this.header[a].slice(0,2)//recorta la cadena del día Sa 2 => Sa
                    if(dia === 'Sa' || dia === 'Do'){                                        
                        alumno.splice(a, 0, ' ')
                        /*IMPORTANTE SUMAR +1 a lengthAlumno para que el for itere en todo el length*/
                        lengthAlumno++
                    }
                }
                /*Calcula la diferencia entre el length de la fila y el de cada alumno, y luego añade / para completar
                la fila
                */
                result = lengthRow - lengthAlumno
                for(let b = 0; b < result; b++){                    
                    alumno.push(this.filler)
                }
                //Luego añade el numero de asistencias y faltas para completar la fila
                alumno.push(numberAsis[0], numberAsis[1])              
                // va guardando el array alumno dentro del array readyAlumnos
                readyAlumnos.push(alumno)
            })
            //finalmente pushea el array de array readyAlumnos en body
            this.body = readyAlumnos
        }
    }
}
</script>

<style>
.reportePlanillaAsis{    
    width: 100%;    
    margin-bottom: 50px;          
    border-collapse: collapse;    
    background-color: white;
    text-align: center;
    box-shadow: 0 0 5px rgb(99, 99, 99);
}
.reportePlanillaAsis__tr{      
    border-bottom: 1px solid rgba(0,0,0,.12);
    transition: all .2s;
}
.reportePlanillaAsis__tr:hover{
    background-color: #EEEEEE;
}

.reportePlanillaAsis__td{    
    padding: 10px 2px;    
}
.reportePlanillaAsis__td:first-child{
    padding-left: 10px;
    text-align: left;
}
.reportePlanillaAsis__td--head{    
    padding-bottom: 20px;
}
</style>
```

# Como usar
``` bash
<template>
  <div>
    <planilla-asistencia :year="2018" :alumnos="alumnos" :month="5" filler=" "></planilla-asistencia>
  </div>
</template>

<script>
import planillaAsistencia from './reportePlanillaAsis.vue'
export default {
  name: 'HelloWorld',
  components: {
    planillaAsistencia  
  },
  data () {
    return {
      alumnos: [
        {
          apellidoAlumno: 'Rei',
          asistencias: 10,
          inasistencias: 6,
          nameAlumno: 'Ayanami',
          registroAsistencia: [
            { value: 'P' },
            { value: 'A' },
            { value: 'P' },
            { value: 'A' },
            { value: 'A' },
            { value: 'P' },
            { value: 'P' },
            { value: 'A' },
            { value: 'P' },
            { value: 'P' },
            { value: 'A' },
            { value: 'P' },
            { value: 'P' },
            { value: 'A' },
            { value: 'P' },
            { value: 'P' },
          ]
        },
        {
          apellidoAlumno: 'Ikari',
          asistencias: 4,
          inasistencias: 1,
          nameAlumno: 'Shinji',
          registroAsistencia: [
            { value: 'P' },
            { value: 'P' },
            { value: 'P' },
            { value: 'P' },
            { value: 'A' }
          ]
        }
        
      ]
    }
  }
}
</script>
```

## Explicación

``` bash
# props
"year" indica el año

"alumnos" contiene los datos de los alumnos para ser consumidos por el
componente (el objeto a pasar debe ser tal cual se muestra en la instancia
padre)

"month" indica el mes actual o uno de su elección

"filler" indica como quieres que se rellenen las celdas vacias, puede ser
cualquier string como / * + o incluso un espacio
```


